package handler

import (
	"log"

	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	//"github.com/micro/go-micro/errors"

	"sync"

	"context"

	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

const (
	ROUTING_KEY_EMAIL_NEW = "email_new"
	EXCHANGE              = ""
	MANDATORY             = false
	DURABLE               = true
	AUTODELETE            = false
	EXCLUSIVE             = false
	NOWAIT                = false
	IMMEDIATE             = false
	AUTOACK               = false
	NOLOCAL               = false
)

var queues = map[string]bool{ROUTING_KEY_EMAIL_NEW: true}
var processingQueues = map[string]bool{}

type Queue interface {
	Publish(email *email.Email) error
	Consume(ctx context.Context, queue string, emails chan<- *email.Email, handler func(*email.Email) bool)
	//StartProcessing(queue string) error
	//StopProcessing(queue string) error
}

type RabbitMQ struct {
	Conn *amqp.Connection
}

func (r *RabbitMQ) StartProcessing(queue string) error {
	var mutex = &sync.Mutex{}

	if _, ok := queues[queue]; !ok {
		log.Fatalf("Queue is not registered")
		return errors.New("Queue is not registered")
	}
	mutex.Lock()
	processingQueues[queue] = true
	mutex.Unlock()

	return nil
}

func (r *RabbitMQ) Publish(email *email.Email) error {

	ch, err := r.Conn.Channel()
	if err != nil {
		return err
	}
	defer func() {
		err := ch.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	// TODO: determine key based on priority
	routingKey := ROUTING_KEY_EMAIL_NEW

	q, err := ch.QueueDeclare(
		routingKey, // name
		DURABLE,
		AUTODELETE,
		EXCLUSIVE,
		NOWAIT,
		nil,
	)
	if err != nil {
		return err
	}


	data, err := proto.Marshal(email)
	if err != nil {
		return err
	}

	err = ch.Publish(
		EXCHANGE,
		q.Name, // routing key
		MANDATORY,
		IMMEDIATE,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(data),
		})
	if err != nil {
		return err
	}

	return nil
}

func (r *RabbitMQ) Consume(ctx context.Context, queue string, emails chan<- *email.Email, handler func(*email.Email) bool) {

	if _, ok := queues[queue]; !ok {
		log.Fatalf("Queue is not registered")
		return
	}

	ch, err := r.Conn.Channel()
	if err != nil {
		log.Fatalf("Failed to create a channel: %s", err)
		return
	}
	defer func() {
		err := ch.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()
	q, err := ch.QueueDeclare(
		queue,
		DURABLE,
		AUTODELETE,
		EXCLUSIVE,
		NOWAIT,
		nil, // arguments
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %s", err)
		return
	}

	msgs, err := ch.Consume(
		q.Name,
		EXCHANGE,
		AUTOACK,
		EXCLUSIVE,
		NOLOCAL,
		NOWAIT,
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %s", err)
		return
	}

	stopProcessing := make(chan bool, 1)

	go func() {


		for {
			select {
			case <-stopProcessing:
				return
			case msg := <-msgs:

				newEmail := &email.Email{}

				err := proto.Unmarshal(msg.Body, newEmail)
				if err != nil {
					log.Fatal("RABBITMQ handler, unmarshaling error: ", err)
				}

				if handler(newEmail) {
					msg.Ack(false)
					emails <- newEmail
					continue
				} else {
					msg.Nack(false, false)
				}
			}
		}

		//for d := range msgs {
		//	select {
		//	case <-stopProcessing:
		//		fmt.Println("RABBITMQ handler, received stop in RMQ.Consume")
		//		return
		//	default:
		//		log.Printf("RabbitMQ handler: Received a message: %s", d.Body)
		//
		//		newEmail := &email.Email{}
		//
		//		err := proto.Unmarshal(d.Body, newEmail)
		//		if err != nil {
		//			log.Fatal("RABBITMQ handler, unmarshaling error: ", err)
		//		}
		//		fmt.Printf("RABBITMQ handler, before sending email to channel: %+v \n", newEmail)
		//
		//		if handler(newEmail) {
		//			fmt.Printf("RABBITMQ handler, after sending email to channel: %+v \n", newEmail)
		//			d.Ack(false)
		//			emails <- newEmail
		//			continue
		//		} else {
		//			fmt.Printf("RABBITMQ handler, failed to handle sending: %+v \n", newEmail)
		//		}
		//	}
		//}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")

	select {
	case <-ctx.Done():
		close(emails)
	}

	stopProcessing <- true

}
