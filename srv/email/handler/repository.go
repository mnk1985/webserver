package handler

import (
	"log"

	"errors"

	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	"github.com/jinzhu/gorm"
)

type Repository interface {
	Get(email *email.Email) (*email.Email, error)
	Create(email *email.Email) error
	Update(email *email.Email) error
	Delete(email *email.Email) error
}

type EmailRepository struct {
	Db *gorm.DB
}

func (repo *EmailRepository) Get(camp *email.Email) (*email.Email, error) {
	log.Printf("Retrieving email: %+v", camp)
	if err := repo.Db.First(&camp).Error; err != nil {
		return nil, err
	}
	return camp, nil
}

func (repo *EmailRepository) Create(camp *email.Email) error {
	log.Printf("Creating email: %+v", camp)

	if err := repo.Db.Create(camp).Error; err != nil {
		return err
	}
	return nil
}

func (repo *EmailRepository) Update(camp *email.Email) error {
	log.Printf("Updating email: %+v", camp)
	if id := camp.Id; id == "" {
		return errors.New("no email.id provided")
	}

	if err := repo.Db.First(&camp).Updates(camp).Error; err != nil {
		return err
	}
	return nil
}

func (repo *EmailRepository) Delete(camp *email.Email) error {
	log.Printf("Deleting email: %+v", camp)
	if err := repo.Db.First(&camp).Delete(&camp).Error; err != nil {
		return err
	}
	return nil
}
