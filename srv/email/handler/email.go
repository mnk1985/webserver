package handler

import (
	"context"
	"fmt"
	"os/signal"
	"syscall"
	"time"

	"errors"

	"os"

	senderSrv "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"
	emailSrv "bitbucket.org/mnk1985/webserver/srv/email/proto"
	"github.com/micro/go-log"
	"github.com/nu7hatch/gouuid"
)

type Email struct {
	Repo   EmailRepositoryInterface
	Queue  Queuer
	Sender senderSrv.EmailSenderServiceClient
}

// TODO: change naming to conventional
type EmailRepositoryInterface interface {
	Get(email *emailSrv.Email) (*emailSrv.Email, error)
	Create(email *emailSrv.Email) error
	Update(email *emailSrv.Email) error
	Delete(email *emailSrv.Email) error
}

type Queuer interface {
	StartProcessing(queue string) error
	Publish(email *emailSrv.Email) error
	Consume(ctx context.Context, queue string, emails chan<- *emailSrv.Email, handler func(*emailSrv.Email) bool)
}

const WORKER_KEEP_ALIVE = 20 // in seconds

func (e *Email) Create(ctx context.Context, req *emailSrv.Email, rsp *emailSrv.EmailResponse) error {

	uid, err := uuid.NewV4()
	log.Log("uid generated \n")
	if err != nil {
		return err
	}
	req.Id = uid.String()
	log.Log("uid attached to email \n")

	err = e.Repo.Create(req)
	if err != nil {
		return err
	}
	rsp.Email = req
	log.Log("email created in repo")

	err = e.Queue.Publish(req)
	if err != nil {
		return err
	}
	log.Log("email (should be) published")

	return nil
}

func (e *Email) Update(ctx context.Context, req *emailSrv.Email, rsp *emailSrv.EmailResponse) error {
	err := e.Repo.Update(req)
	if err != nil {
		return err
	}
	rsp.Email = req
	return nil
}

func (e *Email) Delete(ctx context.Context, req *emailSrv.Email, rsp *emailSrv.Error) error {
	log.Log("Received Email.Delete request")

	err := e.Repo.Delete(req)

	if err != nil {
		return err
	}

	return nil
}

func (e *Email) Get(ctx context.Context, req *emailSrv.Email, rsp *emailSrv.EmailResponse) error {
	log.Log("Received Email.Get request")

	cmp, err := e.Repo.Get(req)
	if err != nil {
		return err
	}
	rsp.Email = cmp

	return nil
}

func (e *Email) Publish(ctx context.Context, stream emailSrv.EmailService_PublishStream) error {
	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}

		err = e.Queue.Publish(req)

		if err != nil {
			return err
		}
		if err = stream.Send(&emailSrv.EmailResponse{Email: req}); err != nil {
			return err
		}
	}
	return nil
}

func (e *Email) Consume(ctx context.Context, queueName *emailSrv.QueueName, stream emailSrv.EmailService_ConsumeStream) error {


	emails := make(chan *emailSrv.Email)

	ctx, cancel := context.WithCancel(ctx)

	d := time.Now().Add(WORKER_KEEP_ALIVE * time.Second)
	ctx, cancelTimeout := context.WithDeadline(context.Background(), d)
	defer func() {
		cancelTimeout()
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	defer func() {
		signal.Stop(sigChan)
		cancel()
	}()
	go func() {
		select {
		case <-sigChan:
			cancel()
			return
		case <-ctx.Done():
		}
	}()


	senderClient, err := e.Sender.Send(ctx)

	if err != nil {
		return errors.New(fmt.Sprintf("failed to create send client: %+v \n", err))
	}

	handler := func(email *emailSrv.Email) bool {
		err = senderClient.Send(email)
		if err != nil {
			fmt.Println("Process sending, failed to send")
			return false
		}
		return true
	}

	go e.Queue.Consume(ctx, queueName.Name, emails, handler)

	// TODO: catch stream close to return immediately
	for {
		select {
		case msg := <-emails:
			if err := stream.Send(&emailSrv.EmailResponse{Email: msg}); err != nil {
				return err
			}
		}
	}

	return nil
}
