package handler

import (
	go_micro_srv_campaign "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	go_micro_srv_email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	go_micro_srv_profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	"context"
	"fmt"
	"testing"
)

type EmailRepositoryMock struct {
	emails map[string] *go_micro_srv_email.Email
}

func (repo *EmailRepositoryMock) Get(email *go_micro_srv_email.Email) (*go_micro_srv_email.Email, error) {

	email, isExists := repo.emails[email.GetId()]
	if !isExists {
		return nil, fmt.Errorf("email not found")
	}

	return email, nil
}

func (repo *EmailRepositoryMock) Create(email *go_micro_srv_email.Email) error {

	_, isExists := repo.emails[email.GetId()]
	if isExists {
		return fmt.Errorf("email is already added")
	}

	repo.emails[email.GetId()] = email

	return nil
}

func (repo *EmailRepositoryMock) Update(email *go_micro_srv_email.Email) error {

	_, isExists := repo.emails[email.GetId()]
	if !isExists {
		return fmt.Errorf("email not found")
	}

	repo.emails[email.GetId()] = email

	return nil
}

func (repo *EmailRepositoryMock) Delete(email *go_micro_srv_email.Email) error {
	_, isExists := repo.emails[email.GetId()]
	if !isExists {
		return fmt.Errorf("email not found")
	}

	delete(repo.emails, email.GetId())

	return nil
}

type QueuerMock struct {

}
// StartProcessing(queue string) error
//	Publish(go_micro_srv_email *go_micro_srv_email.Email) error
//	Consume(ctx context.Context, queue string, emails chan<- *go_micro_srv_email.Email, handler func(*go_micro_srv_email.Email) bool)
func (*QueuerMock) StartProcessing (queue string) error {
	return nil
}

func (*QueuerMock) Publish (email *go_micro_srv_email.Email) error {
	return nil
}

func (*QueuerMock) Consume(ctx context.Context, queue string, emails chan<- *go_micro_srv_email.Email, handler func(*go_micro_srv_email.Email) bool) {
}

func TestEmail_Create(t *testing.T) {

	repoMock := EmailRepositoryMock{
		emails: make(map[string]*go_micro_srv_email.Email),
	}
	queueMock := QueuerMock{}

	emailService := Email{
		Repo: &repoMock,
		Queue: &queueMock,
	}

	camp := go_micro_srv_campaign.Campaign{
		Id:"cmp1",
		Name:"welcome_email_01",
		Description:"Send after sign up",
	}

	sender := go_micro_srv_profile.Profile{
		Name:"support manager 01",
		Email:"support@example.com",
	}

	req := go_micro_srv_email.Email{
		Subject: "Welcome to the site",
		Content:"Thanks for joining",
		Campaign: &camp,
		Profile: &sender,
	}
	resp := go_micro_srv_email.EmailResponse{

	}

	testCases := []struct{
		req *go_micro_srv_email.Email
		resp *go_micro_srv_email.EmailResponse
		expectedErrMsg string
	}{
		{
			req: &req,
			resp: &resp,
			expectedErrMsg: "",
		},
		// TODO: add more testcases after adding validations
	}

	for _, tc := range testCases {

		err := emailService.Create(context.Background(), tc.req, tc.resp)
		if err != nil && err.Error() != tc.expectedErrMsg {
			t.Errorf("expected error: %s, got: %s \n", tc.expectedErrMsg, err.Error())
		}

		emailFromRepo := repoMock.emails[tc.req.GetId()]

		if emailFromRepo.GetSubject() != tc.req.GetSubject() {
			t.Errorf("invalid subject. should be : %s, got: %s \n", tc.req.GetSubject(), emailFromRepo.GetSubject())
		}

		// TODO: check other fields

	}

}

func TestEmail_Update(t *testing.T) {

	emails := make(map[string]*go_micro_srv_email.Email)
	email := go_micro_srv_email.Email{
		Id:"email_101",
		Subject: "Welcome",
		Content:"Some content",
	}

	emails[email.GetId()] = &email

	repoMock := EmailRepositoryMock{
		emails: emails,
	}
	queueMock := QueuerMock{}

	emailService := Email{
		Repo: &repoMock,
		Queue: &queueMock,
	}

	req := go_micro_srv_email.Email{
		Id:"email_101",
		Subject: "Welcome (updated)",
		Content:"Some content (updated)",
	}
	resp := go_micro_srv_email.EmailResponse{}

	testCases := []struct{
		req *go_micro_srv_email.Email
		resp *go_micro_srv_email.EmailResponse
		expectedErrMsg string
	}{
		{
			req: &req,
			resp: &resp,
			expectedErrMsg: "",
		},
		// TODO: add more testcases after adding validations
	}

	for _, c := range testCases {
		fmt.Printf("%+v \n", c)

		err := emailService.Update(context.Background(), &req, &resp)
		if err != nil && err.Error() != c.expectedErrMsg {
			t.Errorf("error when updating email: %s, got: %s \n", c.expectedErrMsg, err.Error())
		}

		if len(resp.GetErrors()) > 0 {
			t.Errorf("got some errors when creating : %+v \n", resp.GetErrors())
		}

		emailFromRepo := repoMock.emails[c.req.GetId()]

		if emailFromRepo.GetSubject() != c.req.GetSubject() {
			t.Errorf("invalid subject. should be : %s, got: %s \n", c.req.GetSubject(), emailFromRepo.GetSubject())
		}
	}
}

func TestEmail_Get(t *testing.T) {
	emails := make(map[string]*go_micro_srv_email.Email)
	subject := "Welcome"
	email := go_micro_srv_email.Email{
		Id:"email_101",
		Subject: subject,
		Content:"Some content",
	}

	emails[email.GetId()] = &email

	repoMock := EmailRepositoryMock{
		emails: emails,
	}
	queueMock := QueuerMock{}

	emailService := Email{
		Repo: &repoMock,
		Queue: &queueMock,
	}

	req := go_micro_srv_email.Email{
		Id:"email_101",
	}
	resp := go_micro_srv_email.EmailResponse{}

	testCases := []struct{
		req *go_micro_srv_email.Email
		resp *go_micro_srv_email.EmailResponse
		expectedErrMsg string
	}{
		{
			req: &req,
			resp: &resp,
			expectedErrMsg: "",
		},
		// TODO: add more testcases after adding validations
	}

	for _, tc := range testCases {
		fmt.Printf("%+v \n", tc)

		err := emailService.Get(context.Background(), &req, &resp)
		if err != nil && err.Error() != tc.expectedErrMsg {
			t.Errorf("error when getting email: %s, got: %s \n", tc.expectedErrMsg, err.Error())
		}

		if len(resp.GetErrors()) > 0 {
			t.Errorf("got some errors when getting : %+v \n", resp.GetErrors())
		}

		if resp.Email.GetSubject() != subject {
			t.Errorf("invalid subject. should be : %s, got: %s \n", subject, resp.Email.GetSubject())
		}
	}
}

func TestEmail_Delete(t *testing.T) {
	emails := make(map[string]*go_micro_srv_email.Email)
	subject := "Welcome"
	email := go_micro_srv_email.Email{
		Id:"email_101",
		Subject: subject,
		Content:"Some content",
	}

	emails[email.GetId()] = &email

	repoMock := EmailRepositoryMock{
		emails: emails,
	}
	queueMock := QueuerMock{}

	emailService := Email{
		Repo: &repoMock,
		Queue: &queueMock,
	}

	req := go_micro_srv_email.Email{
		Id:"email_101",
	}
	resp := go_micro_srv_email.Error{}
	expectedErrResponse := go_micro_srv_email.Error{}

	testCases := []struct{
		req *go_micro_srv_email.Email
		resp *go_micro_srv_email.Error
		expectedErrMsg string
		expectedErrResp *go_micro_srv_email.Error
	}{
		{
			req: &req,
			resp: &resp,
			expectedErrMsg: "",
			expectedErrResp: &expectedErrResponse,
		},
		// TODO: add more testcases after adding validations
	}

	for _, tc := range testCases {
		fmt.Printf("%+v \n", tc)

		err := emailService.Delete(context.Background(), &req, &resp)
		if err != nil && err.Error() != tc.expectedErrMsg {
			t.Errorf("error when deleting email: %s, got: %s \n", tc.expectedErrMsg, err.Error())
		}

		if tc.expectedErrResp.GetCode() != resp.GetCode() {
			t.Errorf("error response code does not match. should be: %d, got: %d \n", tc.expectedErrResp.GetCode(), resp.GetCode())
		}

		if tc.expectedErrResp.GetDescription() != resp.GetDescription() {
			t.Errorf("error response description does not match. should be: %s, got: %s \n", tc.expectedErrResp.GetDescription(), resp.GetDescription())
		}

		if len(repoMock.emails) > 0 {
			t.Errorf("repoMock.emails should be empty, got: %d \n", len(repoMock.emails))
		}

	}
}