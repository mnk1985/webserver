package main

import (
	"bitbucket.org/mnk1985/webserver/srv/email/handler"
	"bitbucket.org/mnk1985/webserver/srv/utils/rabbitmq"
	"github.com/micro/go-log"
	"github.com/micro/go-micro"

	"fmt"

	senderSrv "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"
	emailSrv "bitbucket.org/mnk1985/webserver/srv/email/proto"
	"bitbucket.org/mnk1985/webserver/srv/utils/database"
	//"bitbucket.org/mnk1985/webserver/srv/utils/rabbitmq"
)

func main() {

	Db, err := database.CreateConnection()
	if err != nil {
		log.Fatalf("Could not connect to DB: %v\n", err)
	}
	defer func() {
		err := Db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	Rmq, err := rabbitmq.CreateConnection()
	if err != nil {
		log.Fatalf("Could not connect to RMQ: %v\n", err)
	}
	defer func() {
		err := Rmq.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	fmt.Println("migrating db")
	Db.AutoMigrate(&emailSrv.Email{})

	Repository := &handler.EmailRepository{Db: Db}

	Queue := &handler.RabbitMQ{Conn: Rmq}

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.email"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	emailSrv.RegisterEmailServiceHandler(service.Server(), &handler.Email{
		Repo:   Repository,
		Queue:  Queue,
		Sender: senderSrv.NewEmailSenderServiceClient("go.micro.srv.email-sender", service.Client()),
	})

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
