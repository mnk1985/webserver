package handler

import (
	"context"

	campaign "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	uuid "github.com/nu7hatch/gouuid"
)

type CampRepoInterface interface {
	Get(campaign *campaign.Campaign) (*campaign.Campaign, error)
	Create(campaign *campaign.Campaign) error
	Update(campaign *campaign.Campaign) error
	Delete(campaign *campaign.Campaign) error
}

type Campaign struct {
	Repo CampRepoInterface
}

func (camp *Campaign) Create(ctx context.Context, req *campaign.Campaign, rsp *campaign.CampaignResponse) error {

	uid, err := uuid.NewV4()
	if err != nil {
		return err
	}
	req.Id = uid.String()
	err = camp.Repo.Create(req)
	if err != nil {
		return err
	}
	rsp.Campaign = req
	return nil
}

func (camp *Campaign) Update(ctx context.Context, req *campaign.Campaign, rsp *campaign.CampaignResponse) error {
	err := camp.Repo.Update(req)
	if err != nil {
		return err
	}
	rsp.Campaign = req
	return nil
}

func (camp *Campaign) Delete(ctx context.Context, req *campaign.Campaign, rsp *campaign.Error) error {
	err := camp.Repo.Delete(req)

	if err != nil {
		return err
	}

	return nil
}

func (camp *Campaign) Get(ctx context.Context, req *campaign.Campaign, rsp *campaign.CampaignResponse) error {
	cmp, err := camp.Repo.Get(req)
	if err != nil {
		return err
	}
	rsp.Campaign = cmp

	return nil
}
