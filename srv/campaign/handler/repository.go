package handler

import (
	"errors"

	pb "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	"github.com/jinzhu/gorm"
)

type DB interface {
	First(out interface{}, where ...interface{}) *DB
	Create(value interface{}) *DB
	Updates(values interface{}, ignoreProtectedAttrs ...bool) *DB
	Delete(value interface{}, where ...interface{}) *DB
}

type CampaignRepository struct {
	Db *gorm.DB
}

func (repo *CampaignRepository) Get(camp *pb.Campaign) (*pb.Campaign, error) {
	if err := repo.Db.First(&camp).Error; err != nil {
		return nil, err
	}
	return camp, nil
}

func (repo *CampaignRepository) Create(camp *pb.Campaign) error {
	if err := repo.Db.Create(camp).Error; err != nil {
		return err
	}
	return nil
}

func (repo *CampaignRepository) Update(camp *pb.Campaign) error {
	if id := camp.Id; id == "" {
		return errors.New("no campaign.id provided")
	}

	if err := repo.Db.First(&camp).Updates(camp).Error; err != nil {
		return err
	}
	return nil
}

func (repo *CampaignRepository) Delete(camp *pb.Campaign) error {
	if err := repo.Db.First(&camp).Delete(&camp).Error; err != nil {
		return err
	}
	return nil
}
