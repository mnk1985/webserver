package handler

import (
	"context"
	"fmt"
	"testing"

	cmp "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
)

type MockCampRepo struct {
	campaigns map[string]*cmp.Campaign
}

type FailCheckRepo struct {
}

func (repo *MockCampRepo) Create(camp *cmp.Campaign) error {

	repo.campaigns[camp.Id] = camp

	return nil
}

func (repo *MockCampRepo) Get(camp *cmp.Campaign) (*cmp.Campaign, error) {

	campForGet, ok := repo.campaigns[camp.Id]
	if !ok {
		return nil, fmt.Errorf("campaign not found: %+v \n", camp)
	}

	return campForGet, nil
}

func (repo *MockCampRepo) Update(camp *cmp.Campaign) error {

	campForUpdate, ok := repo.campaigns[camp.Id]
	if !ok {
		return fmt.Errorf("campaign not found: %+v \n", camp)
	}

	campForUpdate.Name = camp.Name
	campForUpdate.Description = camp.Description
	campForUpdate.Profile = camp.Profile

	return nil
}

func (repo *MockCampRepo) Delete(camp *cmp.Campaign) error {
	_, ok := repo.campaigns[camp.Id]
	if !ok {
		return fmt.Errorf("campaign not found: %+v \n", camp)
	}

	delete(repo.campaigns, camp.Id)

	return nil
}

func (repo *FailCheckRepo) Create(camp *cmp.Campaign) error {
	return fmt.Errorf("error when creating campaign")
}

func (repo *FailCheckRepo) Get(camp *cmp.Campaign) (*cmp.Campaign, error) {
	return nil, fmt.Errorf("error when getting campaign")
}

func (repo *FailCheckRepo) Update(camp *cmp.Campaign) error {
	return fmt.Errorf("error when updating campaign")
}

func (repo *FailCheckRepo) Delete(camp *cmp.Campaign) error {
	return fmt.Errorf("error when deleting campaign")
}

func TestHappyPath(t *testing.T) {

	// create campaign

	campaigns := make(map[string]*cmp.Campaign)

	repo := MockCampRepo{
		campaigns: campaigns,
	}

	campService := Campaign{Repo: &repo}
	profileId := "101"
	prof := profile.Profile{
		Id:       profileId,
		Name:     "Alex",
		Email:    "test@example.com",
		Address:  "",
		Password: "",
	}
	req := cmp.Campaign{
		Name:        "Test name",
		Description: "Test description",
		Profile:     &prof,
	}

	createResp := cmp.CampaignResponse{}

	if err := campService.Create(context.Background(), &req, &createResp); err != nil {
		t.Errorf("error when creating campaign: %+v \n", err)
	}

	campId := createResp.GetCampaign().GetId()

	if campId == "" {
		t.Errorf("empty id for campaign when creating \n")
	}

	// get campaign (validate creating)
	getReq := cmp.Campaign{
		Id: campId,
	}
	getResp := cmp.CampaignResponse{}

	if err := campService.Get(context.Background(), &getReq, &getResp); err != nil {
		t.Errorf("error when getting campaign: %+v \n", err)
	}

	if getResp.GetCampaign().GetName() != "Test name" {
		t.Errorf("wrong expected name: %s \n", getResp.Campaign.Name)
	}

	if getResp.GetCampaign().GetDescription() != "Test description" {
		t.Errorf("wrong expected description: %s \n", getResp.Campaign.Description)
	}

	if getResp.GetCampaign().GetProfile().GetId() != "101" {
		t.Errorf("wrong profileId, expected %s, got: %s \n", profileId, getResp.GetCampaign().GetProfile().GetId())
	}

	// update campaign
	updateReq := cmp.Campaign{
		Id:          campId,
		Name:        "Test name (updated)",
		Description: "Test description (updated)",
	}
	updateResp := cmp.CampaignResponse{}

	if err := campService.Update(context.Background(), &updateReq, &updateResp); err != nil {
		t.Errorf("error when updating campaign: %+v \n", err)
	}

	if updateResp.GetCampaign().GetName() != "Test name (updated)" {
		t.Errorf("wrong expected name: %s \n", updateResp.GetCampaign().Name)
	}

	if updateResp.GetCampaign().GetDescription() != "Test description (updated)" {
		t.Errorf("wrong expected description: %s \n", updateResp.Campaign.Description)
	}

	//get campaign  (validate updating)

	getReq = cmp.Campaign{
		Id: campId,
	}
	getResp = cmp.CampaignResponse{}

	if err := campService.Get(context.Background(), &getReq, &getResp); err != nil {
		t.Errorf("error when geting campaign: %+v \n", err)
	}

	if getResp.GetCampaign().GetName() != "Test name (updated)" {
		t.Errorf("wrong expected name: %s \n", getResp.GetCampaign().Name)
	}

	if getResp.GetCampaign().GetDescription() != "Test description (updated)" {
		t.Errorf("wrong expected description: %s \n", getResp.GetCampaign().Description)
	}

	// delete campaign

	deleteReq := cmp.Campaign{
		Id: campId,
	}

	deleteError := cmp.Error{}

	if err := campService.Delete(context.Background(), &deleteReq, &deleteError); err != nil {
		t.Errorf("error when deleting campaign: %+v \n", err)
	}

	if deleteError.Code != 0 {
		t.Errorf("error response when deleting campaign: %+v \n", deleteError)
	}

}

func TestCreateFail(t *testing.T) {
	campSrv := Campaign{Repo: &FailCheckRepo{}}

	err := campSrv.Create(context.Background(), &cmp.Campaign{}, &cmp.CampaignResponse{})

	if err == nil {
		t.Errorf("should be error when campaign is not created \n")
	}

	if err.Error() != "error when creating campaign" {
		t.Errorf("incorrect error message: %s \n", err)
	}
}

func TestUpdateFail(t *testing.T) {
	campSrv := Campaign{Repo: &FailCheckRepo{}}

	err := campSrv.Update(context.Background(), &cmp.Campaign{}, &cmp.CampaignResponse{})

	if err == nil {
		t.Errorf("should be error when campaign is not created \n")
	}

	if err.Error() != "error when updating campaign" {
		t.Errorf("incorrect error message: %s \n", err)
	}
}

func TestGetFail(t *testing.T) {
	campSrv := Campaign{Repo: &FailCheckRepo{}}

	err := campSrv.Get(context.Background(), &cmp.Campaign{}, &cmp.CampaignResponse{})

	if err == nil {
		t.Errorf("should be error when campaign is not created \n")
	}

	if err.Error() != "error when getting campaign" {
		t.Errorf("incorrect error message: %s \n", err)
	}
}

func TestDeleteFail(t *testing.T) {
	campSrv := Campaign{Repo: &FailCheckRepo{}}

	err := campSrv.Delete(context.Background(), &cmp.Campaign{}, &cmp.Error{})

	if err == nil {
		t.Errorf("should be error when campaign is not created \n")
	}

	if err.Error() != "error when deleting campaign" {
		t.Errorf("incorrect error message: %s \n", err)
	}
}
