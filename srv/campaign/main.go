package main

import (
	"bitbucket.org/mnk1985/webserver/srv/campaign/handler"
	"github.com/micro/go-log"
	"github.com/micro/go-micro"

	campaign "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	"bitbucket.org/mnk1985/webserver/srv/utils/database"
)

func main() {

	Db, err := database.CreateConnection()
	defer func() {
		err := Db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	if err != nil {
		log.Fatalf("Could not connect to DB: %v", err)
	}

	Db.AutoMigrate(&campaign.Campaign{})

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.campaign"),
	)

	// Register Handler
	campaign.RegisterCampaignServiceHandler(service.Server(), &handler.Campaign{Repo: &handler.CampaignRepository{Db: Db}})

	// Initialise service
	service.Init()

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
