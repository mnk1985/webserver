package handler

import (
	"fmt"
	"log"

	"errors"

	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	uuid "github.com/nu7hatch/gouuid"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
)

type Service struct {
	Repo *ProfileRepository
}

func (srv *Service) GetByCreds(ctx context.Context, creds *profile.ProfileCredentials, res *profile.Response) error {
	prof, err := srv.Repo.GetByEmail(creds.Email)
	if err != nil {
		return err
	}
	if err := bcrypt.CompareHashAndPassword([]byte(prof.Password), []byte(creds.Password)); err != nil {
		fmt.Printf("password hash check failed, %+v", err)
		return err
	}

	res.Profile = prof
	return nil
}

func (srv *Service) GetById(ctx context.Context, id *profile.Id, res *profile.Response) error {
	prof, err := srv.Repo.GetById(id.Id)
	if err != nil {
		return err
	}
	res.Profile = prof
	return nil
}

func (srv *Service) Create(ctx context.Context, user *profile.Profile, res *profile.Response) error {

	uid, err := uuid.NewV4()
	if err != nil {
		log.Fatal(err)
	}
	user.Id = uid.String()
	res.Profile = user

	// Generates a hashed version of our password
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.New(fmt.Sprintf("error hashing password: %v", err))
	}

	user.Password = string(hashedPass)

	err = srv.Repo.Create(user)
	if err != nil {
		return err
	}

	return nil
}
