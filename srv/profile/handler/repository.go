package handler

import (
	"log"

	pb "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	"github.com/jinzhu/gorm"
)

type Repository interface {
	Get(id string) (*pb.Profile, error)
	Create(profile *pb.Profile) error
	GetById(id string) (*pb.Profile, error)
	GetByEmail(email string) (*pb.Profile, error)
}

type ProfileRepository struct {
	Db *gorm.DB
}

func (repo *ProfileRepository) GetById(id string) (*pb.Profile, error) {
	var user *pb.Profile
	user.Id = id // TODO: fix
	if err := repo.Db.First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *ProfileRepository) Get(id string) (*pb.Profile, error) {
	return repo.GetById(id)
}

func (repo *ProfileRepository) GetByEmail(email string) (*pb.Profile, error) {
	user := &pb.Profile{}
	if err := repo.Db.Where("email = ?", email).
		First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *ProfileRepository) Create(user *pb.Profile) error {
	log.Printf("Creating user: %+v", user)

	if err := repo.Db.Create(user).Error; err != nil {
		return err
	}
	return nil
}
