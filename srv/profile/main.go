package main

import (
	"fmt"
	"log"

	"bitbucket.org/mnk1985/webserver/srv/profile/handler"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	"bitbucket.org/mnk1985/webserver/srv/utils/database"
	"github.com/micro/go-micro"
)

func main() {

	Db, err := database.CreateConnection()
	defer func() {
		err := Db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	if err != nil {
		log.Fatalf("Could not connect to DB: %v", err)
	}

	Db.AutoMigrate(&profile.Profile{})

	Repo := &handler.ProfileRepository{Db: Db}

	srv := micro.NewService(
		micro.Name("go.micro.srv.profile"),
	)

	// Parse the command line flags.
	srv.Init()

	// Register handler
	profile.RegisterProfileServiceHandler(srv.Server(), &handler.Service{Repo: Repo})

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}

}
