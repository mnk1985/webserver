package rabbitmq

import (
	"os"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/streadway/amqp"
)

func CreateConnection() (*amqp.Connection, error) {

	// Get RMQ details from environment variables

	user := os.Getenv("RABBITMQ_USER")
	password := os.Getenv("RABBITMQ_PASSWORD")

	url := "amqp://" + user + ":" + password + "@rmq:5672/"

	conn, err := amqp.Dial(url)

	if err != nil {
		return nil, err
	}

	return conn, nil
}
