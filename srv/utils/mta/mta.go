package mta

import (
	"log"
	"os"

	"strconv"

	"bitbucket.org/mnk1985/webserver/srv/email-sender/handler"
	"github.com/pkg/errors"
)

func GetSmtpConnDetails() (*handler.SmtpConnDetails, error) {

	host := os.Getenv("SMTP_HOST")
	port, err := strconv.Atoi(os.Getenv("SMTP_PORT"))
	if err != nil {
		log.Fatalf("failed to convert port to int, %+v", port)
		return nil, err
	}
	user := os.Getenv("SMTP_USER")
	password := os.Getenv("SMTP_PASSWORD")

	if host == "" || port == 0 {
		return nil, errors.New("host and port should not be empty")
	}

	return &handler.SmtpConnDetails{
		Host:     host,
		Port:     port,
		User:     user,
		Password: password}, nil
}
