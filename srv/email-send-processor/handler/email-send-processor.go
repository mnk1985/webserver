package handler

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	emailSendProcessor "bitbucket.org/mnk1985/webserver/srv/email-send-processor/proto"
	senderSrv "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"
	emailSrv "bitbucket.org/mnk1985/webserver/srv/email/proto"
)

type EmailSendProcessor struct {
	Sender senderSrv.EmailSenderServiceClient
	Email  emailSrv.EmailServiceClient
}

func (p *EmailSendProcessor) ProcessSend(ctx context.Context, req *emailSendProcessor.Request, res *emailSendProcessor.Response) error {
	ctx, cancel := context.WithCancel(ctx)
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	defer func() {
		signal.Stop(sigChan)
		cancel()
	}()
	go func() {
		select {
		case <-sigChan:
			cancel()
		case <-ctx.Done():
		}
	}()

	//handler := func(email emailSrv.Email) {
	//	fmt.Println("Process sending, handler, start")
	//
	//}

	return nil
}

func (p *EmailSendProcessor) Stop(ctx context.Context, req *emailSendProcessor.Request, res *emailSendProcessor.Response) error {

	// TODO: implement

	return nil
}
