package main

import (
	emailSendProcessor "bitbucket.org/mnk1985/webserver/srv/email-send-processor/proto"

	"bitbucket.org/mnk1985/webserver/srv/email-send-processor/handler"
	senderSrv "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"
	emailSrv "bitbucket.org/mnk1985/webserver/srv/email/proto"
	"github.com/micro/go-log"
	"github.com/micro/go-micro"
)

func main() {

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.email-sender"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	emailSendProcessor.RegisterEmailSendProcessorServiceHandler(service.Server(),
		&handler.EmailSendProcessor{
			Email:  emailSrv.NewEmailServiceClient("go.micro.srv.email", service.Client()),
			Sender: senderSrv.NewEmailSenderServiceClient("go.micro.srv.email-sender", service.Client()),
		})

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
