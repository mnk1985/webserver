package main

import (
	"fmt"

	"bitbucket.org/mnk1985/webserver/srv/auth/handler"
	auth "bitbucket.org/mnk1985/webserver/srv/auth/proto"
	"github.com/micro/go-micro"
)

func main() {
	srv := micro.NewService(
		micro.Name("go.micro.srv.auth"),
	)

	// Parse the command line flags.
	srv.Init()

	// Register handler
	auth.RegisterAuthServiceHandler(srv.Server(), &handler.Service{})

	// Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
