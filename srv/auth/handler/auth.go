package handler

import (
	"time"

	"context"

	"errors"

	auth "bitbucket.org/mnk1985/webserver/srv/auth/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	jwt "github.com/dgrijalva/jwt-go"
)

var (
	key = []byte("SuperSecretKey")
)

// CustomClaims is our custom metadata, which will be hashed
// and sent as the second segment in our JWT
type CustomClaims struct {
	Profile *profile.Profile
	jwt.StandardClaims
}

type Service struct {
}

// Decode a token string into a token object
func (srv *Service) Decode(c context.Context, token *auth.Token, resp *auth.ProfileResponse) error {

	// Parse the token
	tok, err := jwt.ParseWithClaims(token.Token, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	if err != nil {
		return err
	}

	// Validate the token and return the custom claims
	if claims, ok := tok.Claims.(*CustomClaims); ok && tok.Valid {
		resp.Profile = claims.Profile
		return nil
	} else {
		return err
	}
}

// Encode a claim into a JWT
func (srv *Service) Encode(c context.Context, profile *profile.Profile, resp *auth.TokenResponse) error {

	expireToken := time.Now().Add(time.Hour * 72).Unix()

	// Create the Claims
	claims := CustomClaims{
		profile,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "go.micro.srv.profile",
		},
	}

	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign token
	signedToken, err := token.SignedString(key)

	resp.Token = signedToken
	//resp.Errors = append(resp.Errors, &auth.Error{Description: err.Error()})

	return err
}

func (srv *Service) ValidateToken(ctx context.Context, req *auth.Token, res *auth.TokenResponse) error {

	profResp := &auth.ProfileResponse{}
	// Decode token
	err := srv.Decode(ctx, req, profResp)

	if err != nil {
		return err
	}

	if profResp.Profile.Id == "" {
		return errors.New("invalid user")
	}

	res.Valid = true

	return nil
}
