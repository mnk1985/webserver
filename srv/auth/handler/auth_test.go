package handler

import (
	"context"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	"testing"
	auth "bitbucket.org/mnk1985/webserver/srv/auth/proto"
)

func TestAll(t *testing.T) {
	var prof = profile.Profile {
		Id:"1111",
		Name: "Alex",
		Email:"email@example.com",
	}

	tokenResponse := auth.TokenResponse{}
	service := Service{}

	// TEST Encode
	err := service.Encode(context.Background(), &prof, &tokenResponse)
	if err != nil {
		t.Errorf("error when encoding token: %+v \n", err)
	}

	tokenString := tokenResponse.Token

	profileResponse := auth.ProfileResponse{}
	authToken := auth.Token{
		Token:tokenString,
	}

	// TEST Decode
	if err := service.Decode(context.Background(), &authToken, &profileResponse); err != nil {
		t.Errorf("error when decodong token: %+v \n", err)
	}

	if len(profileResponse.Errors) > 0 {
		t.Errorf("errors found: %+v \n", profileResponse.Errors)
	}

	if profileResponse.Profile.Id != "1111" {
		t.Errorf("id not match: %+v \n", profileResponse.Profile)
	}

	if profileResponse.Profile.Name != "Alex" {
		t.Errorf("name not match: %+v \n", profileResponse.Profile)
	}

	if profileResponse.Profile.Email != "email@example.com" {
		t.Errorf("email not match: %+v \n", profileResponse.Profile)
	}

	if profileResponse.Profile.Address != "" {
		t.Errorf("address not match: %+v \n", profileResponse.Profile)
	}

	if profileResponse.Profile.Password != "" {
		t.Errorf("password not match: %+v \n", profileResponse.Profile)
	}

	validationResponse := auth.TokenResponse{}

	// TEST Validate
	err = service.ValidateToken(context.Background(), &authToken, &validationResponse)
	if err != nil {
		t.Errorf("error when validating token: %+v \n", err)
	}

	if validationResponse.Valid != true {
		t.Errorf("token is not valid: %+v \n", validationResponse)
	}

	if len(validationResponse.Errors) > 0 {
		t.Errorf("errors when validating token: %+v \n", validationResponse)
	}
}