// Code generated by protoc-gen-go. DO NOT EDIT.
// source: srv/auth/proto/auth.proto

/*
Package go_micro_srv_auth is a generated protocol buffer package.

It is generated from these files:
	srv/auth/proto/auth.proto

It has these top-level messages:
	Token
	TokenResponse
	Error
	ProfileResponse
*/
package go_micro_srv_auth

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import go_micro_srv_profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Token struct {
	Token string `protobuf:"bytes,1,opt,name=token" json:"token,omitempty"`
}

func (m *Token) Reset()                    { *m = Token{} }
func (m *Token) String() string            { return proto.CompactTextString(m) }
func (*Token) ProtoMessage()               {}
func (*Token) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Token) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type TokenResponse struct {
	Token  string   `protobuf:"bytes,1,opt,name=token" json:"token,omitempty"`
	Valid  bool     `protobuf:"varint,2,opt,name=valid" json:"valid,omitempty"`
	Errors []*Error `protobuf:"bytes,3,rep,name=errors" json:"errors,omitempty"`
}

func (m *TokenResponse) Reset()                    { *m = TokenResponse{} }
func (m *TokenResponse) String() string            { return proto.CompactTextString(m) }
func (*TokenResponse) ProtoMessage()               {}
func (*TokenResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *TokenResponse) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

func (m *TokenResponse) GetValid() bool {
	if m != nil {
		return m.Valid
	}
	return false
}

func (m *TokenResponse) GetErrors() []*Error {
	if m != nil {
		return m.Errors
	}
	return nil
}

type Error struct {
	Code        int32  `protobuf:"varint,1,opt,name=code" json:"code,omitempty"`
	Description string `protobuf:"bytes,2,opt,name=description" json:"description,omitempty"`
}

func (m *Error) Reset()                    { *m = Error{} }
func (m *Error) String() string            { return proto.CompactTextString(m) }
func (*Error) ProtoMessage()               {}
func (*Error) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Error) GetCode() int32 {
	if m != nil {
		return m.Code
	}
	return 0
}

func (m *Error) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

type ProfileResponse struct {
	Profile *go_micro_srv_profile.Profile `protobuf:"bytes,1,opt,name=profile" json:"profile,omitempty"`
	Errors  []*Error                      `protobuf:"bytes,2,rep,name=errors" json:"errors,omitempty"`
}

func (m *ProfileResponse) Reset()                    { *m = ProfileResponse{} }
func (m *ProfileResponse) String() string            { return proto.CompactTextString(m) }
func (*ProfileResponse) ProtoMessage()               {}
func (*ProfileResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *ProfileResponse) GetProfile() *go_micro_srv_profile.Profile {
	if m != nil {
		return m.Profile
	}
	return nil
}

func (m *ProfileResponse) GetErrors() []*Error {
	if m != nil {
		return m.Errors
	}
	return nil
}

func init() {
	proto.RegisterType((*Token)(nil), "go.micro.srv.auth.Token")
	proto.RegisterType((*TokenResponse)(nil), "go.micro.srv.auth.TokenResponse")
	proto.RegisterType((*Error)(nil), "go.micro.srv.auth.Error")
	proto.RegisterType((*ProfileResponse)(nil), "go.micro.srv.auth.ProfileResponse")
}

func init() { proto.RegisterFile("srv/auth/proto/auth.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 337 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x52, 0x41, 0x4b, 0xeb, 0x40,
	0x10, 0x6e, 0xda, 0x97, 0xbc, 0xd7, 0x09, 0xe5, 0xe1, 0xe2, 0x21, 0x16, 0x0a, 0x61, 0x4f, 0x3d,
	0x6d, 0xb4, 0x22, 0xea, 0x41, 0x44, 0xb0, 0x20, 0x88, 0x20, 0x51, 0xbc, 0x37, 0xe9, 0xd8, 0x2e,
	0x6d, 0xb3, 0x61, 0x76, 0x1b, 0x2f, 0xfe, 0x6b, 0xff, 0x80, 0x64, 0x93, 0x4a, 0xab, 0xad, 0xf4,
	0xf6, 0xcd, 0xcc, 0xb7, 0xdf, 0x7c, 0x33, 0xb3, 0x70, 0xa4, 0xa9, 0x88, 0x46, 0x4b, 0x33, 0x8d,
	0x72, 0x52, 0x46, 0x59, 0x28, 0x2c, 0x64, 0x07, 0x13, 0x25, 0x16, 0x32, 0x25, 0x25, 0x34, 0x15,
	0xa2, 0x2c, 0x74, 0xaf, 0x13, 0x69, 0x92, 0x65, 0x3a, 0x43, 0x23, 0x14, 0x4d, 0xa2, 0x45, 0x36,
	0x3b, 0xb9, 0xbc, 0x38, 0x8b, 0xde, 0x30, 0xd1, 0x48, 0x05, 0x52, 0x54, 0xaa, 0xe5, 0xa4, 0x5e,
	0xe5, 0x1c, 0x6b, 0xc1, 0x3a, 0xaa, 0x34, 0x79, 0x0f, 0xdc, 0x67, 0x35, 0xc3, 0x8c, 0x1d, 0x82,
	0x6b, 0x4a, 0x10, 0x38, 0xa1, 0xd3, 0x6f, 0xc7, 0x55, 0xc0, 0x17, 0xd0, 0xb1, 0xe5, 0x18, 0x75,
	0xae, 0x32, 0x8d, 0xdb, 0x69, 0x65, 0xb6, 0x18, 0xcd, 0xe5, 0x38, 0x68, 0x86, 0x4e, 0xff, 0x5f,
	0x5c, 0x05, 0xec, 0x18, 0x3c, 0x24, 0x52, 0xa4, 0x83, 0x56, 0xd8, 0xea, 0xfb, 0x83, 0x40, 0xfc,
	0x18, 0x40, 0x0c, 0x4b, 0x42, 0x5c, 0xf3, 0xf8, 0x15, 0xb8, 0x36, 0xc1, 0x18, 0xfc, 0x49, 0xd5,
	0x18, 0x6d, 0x17, 0x37, 0xb6, 0x98, 0x85, 0xe0, 0x8f, 0x51, 0xa7, 0x24, 0x73, 0x23, 0x55, 0x66,
	0x5b, 0xb5, 0xe3, 0xf5, 0x14, 0x7f, 0x87, 0xff, 0x8f, 0xd5, 0x74, 0x5f, 0x7e, 0xcf, 0xe1, 0x6f,
	0x3d, 0xb0, 0xd5, 0xf2, 0x07, 0xbd, 0x4d, 0x13, 0xab, 0x6d, 0xac, 0xde, 0xad, 0xd8, 0x6b, 0xe6,
	0x9b, 0xfb, 0x99, 0x1f, 0x7c, 0x38, 0xe0, 0xdf, 0x2c, 0xcd, 0xf4, 0x09, 0xa9, 0x90, 0x29, 0xb2,
	0x7b, 0xf0, 0x86, 0x99, 0x75, 0xfe, 0x7b, 0xcf, 0x6e, 0xb8, 0x45, 0x7a, 0x63, 0xeb, 0xbc, 0xc1,
	0xee, 0xc0, 0xbb, 0x45, 0x2b, 0x16, 0xec, 0x62, 0x77, 0xf9, 0x96, 0xca, 0xb7, 0x7d, 0xf0, 0x06,
	0x7b, 0x80, 0xce, 0x4b, 0x79, 0x9e, 0x91, 0xc1, 0xea, 0xf2, 0xbb, 0x05, 0xf7, 0x30, 0x96, 0x78,
	0xf6, 0x1f, 0x9d, 0x7e, 0x06, 0x00, 0x00, 0xff, 0xff, 0xcd, 0xd1, 0xae, 0xea, 0xb8, 0x02, 0x00,
	0x00,
}
