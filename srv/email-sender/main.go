package main

import (
	strconv "strconv"

	emailSender "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"

	"os"

	"bitbucket.org/mnk1985/webserver/srv/email-sender/handler"
	"bitbucket.org/mnk1985/webserver/srv/utils/mta"
	"github.com/micro/go-log"
	"github.com/micro/go-micro"
)

func main() {

	// TODO: refactor! do not send real email, use mailhog

	smtpConnDetails, err := mta.GetSmtpConnDetails()
	if err != nil {
		log.Fatalf("Could not connect to DB: %v", err)
	}

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.srv.email-sender"),
	)

	// Initialise service
	service.Init()

	doRealSend, err := strconv.ParseBool(os.Getenv("DO_REAL_SEND"))
	if err != nil {
		log.Fatalf("failed to convert DO_REAL_SEND to bool")
	}

	// Register Handler
	emailSender.RegisterEmailSenderServiceHandler(service.Server(), &handler.EmailSender{
		ConnDetails: *smtpConnDetails,
		DoRealSend:  doRealSend,
	})

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
