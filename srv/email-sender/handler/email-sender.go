package handler

import (
	"context"
	"log"
	"time"

	"fmt"

	emailSender "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"
	"gopkg.in/gomail.v2"
)

type SmtpConnDetails struct {
	Host     string
	Port     int
	User     string
	Password string
}

type EmailSender struct {
	ConnDetails SmtpConnDetails
	DoRealSend  bool
}

func (s *EmailSender) Send(ctx context.Context, stream emailSender.EmailSenderService_SendStream) error {

	ch := make(chan *gomail.Message)

	go func() {

		d := gomail.NewDialer(s.ConnDetails.Host, s.ConnDetails.Port, s.ConnDetails.User, s.ConnDetails.Password)

		var s gomail.SendCloser
		var err error
		open := false
		for {
			select {
			case m, ok := <-ch:
				if !ok {
					return
				}
				if !open {
					if s, err = d.Dial(); err != nil {
						panic(err)
					}
					open = true
				}
				if err := gomail.Send(s, m); err != nil {
					log.Print(err)
				}
				// Close the connection to the SMTP server if no email was sent in
				// the last 30 seconds.
			case <-time.After(30 * time.Second):
				if open {
					if err := s.Close(); err != nil {
						panic(err)
					}
					open = false
				}
			}
		}
	}()

	// Use the channel in your program to send emails.
	for {
		email, err := stream.Recv()
		if err != nil {
			return err
		}
		m := gomail.NewMessage()
		m.SetHeader("From", "verygooddate@gmail.com")
		m.SetHeader("To", email.ToEmail)
		m.SetHeader("Subject", email.Subject)
		m.SetBody("text/html", email.Content)

		if s.DoRealSend {
			ch <- m
		}

		if err = stream.Send(&emailSender.SendResponse{Email: email}); err != nil {
			return err
		}
	}

	// TODO: fix
	//Close the channel to stop the mail daemon.
	close(ch)

	return nil
}
