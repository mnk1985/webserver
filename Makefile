.PHONY: clean proto dep binary build

proto:
	for d in api srv; do \
		for f in $$d/**/proto/*.proto; do \
			protoc --proto_path=$$GOPATH/src:. --micro_out=. --go_out=. $$f; \
			echo compiled: $$f; \
		done \
	done

lint:
	./bin/lint.sh

build:
	./bin/build.sh

clear:
	make clear-docker
	make clear-proto
	make clear-exec

clear-exec:
	find srv api client -executable -type f -delete

clear-proto:
	find srv api -name *micro\.go -delete
	find srv api -name *pb\.go -delete

clear-docker:
	docker ps -a -q | xargs docker rm
	docker images | grep "^webserver_" | awk '{print $$3}' | xargs docker rmi

dep:
	dep ensure -update

run:
	docker-compose build
	docker-compose up

rerun:
	make clear && make proto && make dep && make binary && make run

# generate binary files
exec-srv:
	docker run --rm \
        -v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
        -w /go/src/bitbucket.org/mnk1985/webserver/srv/profile \
        iron/go:dev \
        go build -gcflags "all=-N -l" -o srv-profile .
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/srv/auth \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o srv-auth .
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/srv/campaign \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o srv-campaign .
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/srv/email \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o srv-email .
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/srv/email-sender \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o srv-email-sender .

exec-api:
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/api/account \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o api-account .
	docker run --rm \
        -v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
        -w /go/src/bitbucket.org/mnk1985/webserver/api/campaign \
        iron/go:dev \
        go build -gcflags "all=-N -l" -o api-campaign .
	docker run --rm \
        -v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
        -w /go/src/bitbucket.org/mnk1985/webserver/api/email \
        iron/go:dev \
        go build -gcflags "all=-N -l" -o api-email .

exec-client:
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/client/account \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o client-account .

exec-worker:
	docker run --rm \
		-v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
		-w /go/src/bitbucket.org/mnk1985/webserver/worker/email-send \
		iron/go:dev \
		go build -gcflags "all=-N -l" -o email-send .

exec:
	make exec-srv
	make exec-api
	make exec-client
	make exec-worker

show-exec:
	find . -perm /111 -type f | sed 's#^./##' | sort | grep -v git | grep -v bin
