package main

import (
	"context"

	handler "bitbucket.org/mnk1985/webserver/worker/email-send/handler"

	emailSender "bitbucket.org/mnk1985/webserver/srv/email-sender/proto"

	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	"github.com/micro/go-micro"
)

func main() {

	ctx := context.Background()

	service := micro.NewService(
		micro.Name("go.micro.worker.email"),
	)
	service.Init()

	emailSrv := email.NewEmailServiceClient("go.micro.srv.email", service.Client())
	senderSrv := emailSender.NewEmailSenderServiceClient("go.micro.srv.email-sender", service.Client())

	worker := handler.EmailSendWorker{
		Email:  emailSrv,
		Sender: senderSrv,
	}

	worker.ProcessNewEmails(ctx)
}
