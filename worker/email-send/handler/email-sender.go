package handler

import (
	"context"
	"fmt"
	"os"

	emailHandler "bitbucket.org/mnk1985/webserver/srv/email/handler"
	//"google.golang.org/grpc/metadata"

	"os/signal"

	"syscall"

	"bitbucket.org/mnk1985/webserver/srv/email-sender/proto"
	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
)

type EmailSendWorker struct {
	Sender go_micro_srv_email.EmailSenderServiceClient
	Email  email.EmailServiceClient
}

func (s *EmailSendWorker) ProcessNewEmails(ctx context.Context) {

	// trap Ctrl+C and call cancel on the context
	ctx, cancel := context.WithCancel(ctx)
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	stream, err := s.Email.Consume(ctx, &email.QueueName{Name: emailHandler.ROUTING_KEY_EMAIL_NEW})

	if err != nil {
		fmt.Printf("err when creating stream: %+v \n", err)
		return
	}

	defer func(stream email.EmailService_ConsumeClient) {
		signal.Stop(sigChan)
		cancel()
	}(stream)
	go func() {
		select {
		case <-sigChan:
			//stream.SendMsg(struct{}{})
			if err := stream.Close(); err != nil {
				//
			}
			cancel()
		case <-ctx.Done():
		}
	}()

	for {
		_, err := stream.Recv()
		if err != nil {
			fmt.Printf("err when receiving response from stream: %+v \n", err)
			return
		}
	}


}
