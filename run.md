
# what to install/setup
- install go tools ( https://golang.org/doc/install?download=go1.12.7.linux-amd64.tar.gz )
- setup variables ( https://golang.org/doc/code.html#GOPATH )
# add to ~/.profile
- export PATH=$PATH:/usr/local/go/bin
- export GOPATH=$HOME/www/go
-- \# run go executables globally
-- export PATH=$PATH:$(go env GOPATH)/bin