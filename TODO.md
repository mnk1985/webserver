

# Fixes:
- code review
    + remove not needed prints
    - remove not needed interfaces
    + handle errors in defer
    - fix unreachable statements
    - etc.
- after terminating worker from workers/email-send email is not created when running from client/account
- implement sending with postfix
- cover code with tests
- use github.com/pkg/errors to return errors from services
- use github.com/hashicorp/go-multierror to handle multiple errors [link](https://pocketgophers.com/handling-errors-in-defer/)


# Small:
- build all execs with alpine image (instead of iron/go)

# Features



