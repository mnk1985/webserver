#!/bin/bash

tries=30;
emptyResponse="[]"
while ((tries--)); do
    resp=$(curl consul:8500/v1/catalog/service/go.micro.srv.campaign | xargs);
    if [ "$resp" != "$emptyResponse" ]; then
            echo "Done"
            exit 0
    fi
    sleep 1;
done;

echo 'Tiimeout'
exit 1