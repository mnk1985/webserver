package main

import (
	"context"
	"fmt"

	accountApi "bitbucket.org/mnk1985/webserver/api/account/proto"
	campaignApi "bitbucket.org/mnk1985/webserver/api/campaign/proto"
	emailApi "bitbucket.org/mnk1985/webserver/api/email/proto"
	campaign "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"

	"strconv"

	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/metadata"
)

func main() {
	// Create a new service.
	service := micro.NewService(
		micro.Name("go.micro.client.account"),
	)
	service.Init()

	//Create new client
	accountService := accountApi.NewAccountApiClient("go.micro.api.account", service.Client())

	newProfile := profile.Profile{Email: "verygooddate@gmail.com", Password: "secret3", Name: "Alex Dychka", Address: "some address"}

	response, err := accountService.Create(context.TODO(), &newProfile)

	if err != nil {
		fmt.Println(err)
		return
	}

	prof := response.Profile
	fmt.Printf("created profile: %+v \n", prof)

	//Call the service

	tokenResponse, err := accountService.Auth(context.TODO(), &profile.ProfileCredentials{Email: newProfile.Email, Password: newProfile.Password})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("profile token: %+v\n", tokenResponse.Token)

	token := tokenResponse.Token

	ctxWithToken := metadata.NewContext(context.Background(), map[string]string{
		"token": token,
	})

	profDetailsResponse, err := accountService.Details(ctxWithToken, &accountApi.EmptyRequest{})
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("profile details %+v\n", profDetailsResponse.Profile)

	campService := campaignApi.NewCampaignApiClient("go.micro.api.campaign", service.Client())

	newCampaign := &campaign.Campaign{Name: "test campaign 1", Description: "some description"}

	campaignCreateResponse, err := campService.Create(ctxWithToken, newCampaign)

	if err != nil {
		fmt.Println(err)
		return
	}

	camp := campaignCreateResponse.Campaign
	fmt.Printf("campaign created: %+v\n", camp)

	campaignUpdateResponse, err := campService.Update(ctxWithToken, &campaign.Campaign{Id: camp.Id, Name: "test campaign 2"})

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("campaign updated: %+v\n", campaignUpdateResponse.Campaign)

	campaignDetailsResponse, err := campService.Details(ctxWithToken, &campaignApi.CampaignId{Id: camp.Id})

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("campaign details: %+v\n", campaignDetailsResponse.Campaign)

	emailService := emailApi.NewEmailApiClient("go.micro.api.email", service.Client())

	fmt.Printf("api emailService: %+v", emailService)

	newEmail := email.Email{
		ToEmail:  "dychka.alexandr@gmail.com",
		ToName:   "Alexandr",
		Subject:  "Test email",
		Content:  "<b>Hello</b>, how are you?",
		Priority: email.Email_HIGH,
		Campaign: camp,
	}

	createEmailResponse, err := emailService.Create(ctxWithToken, &newEmail)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("email created: %+v\n", createEmailResponse.Email)

	updateEmailResponse, err := emailService.Update(ctxWithToken, &email.Email{Id: createEmailResponse.Email.Id, Subject: "Edited test email"})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("email updated: %+v\n", updateEmailResponse.Email)

	getEmailResponse, err := emailService.Details(ctxWithToken, &emailApi.EmailId{Id: createEmailResponse.Email.Id})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("email retrieved: %+v\n", getEmailResponse.Email)

	createBulk(emailService, ctxWithToken)

	_, err = emailService.Delete(ctxWithToken, &emailApi.EmailId{Id: createEmailResponse.Email.Id})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("email deleted\n")

	_, err = campService.Delete(ctxWithToken, &campaignApi.CampaignId{Id: camp.Id})
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("campaign deleted\n")
}

func createBulk(cl emailApi.EmailApiClient, ctx context.Context) {

	stream, err := cl.CreateBulk(ctx)
	if err != nil {
		fmt.Println("client-account.createBulk: err:", err)
		return
	}

	// bidirectional stream
	// send and receive messages for a 10 count
	for j := 0; j < 1; j++ {
		subject := "test email subject: " + strconv.Itoa(j)
		content := "test email body: " + strconv.Itoa(j)
		emailSent := &email.Email{Content: content, Subject: subject, ToEmail: "dychka.alexandr@gmail.com"}
		if err := stream.Send(emailSent); err != nil {
			fmt.Println("err:", err)
			return
		}
		rsp, err := stream.Recv()
		if err != nil {
			fmt.Println("recv err", err)
			break
		}
		fmt.Printf("\n client-account.createBulk: Sent msg %v got msg %v \n", emailSent, rsp.Email)
	}

	// close the stream
	if err := stream.Close(); err != nil {
		fmt.Println("stream close err:", err)
	}
}
