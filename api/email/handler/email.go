package handler

import (
	"context"

	emailApi "bitbucket.org/mnk1985/webserver/api/email/proto"

	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	merr "github.com/micro/go-micro/errors"

	"fmt"
)

type Email struct {
	EmailService email.EmailServiceClient
}

func (e *Email) Create(ctx context.Context, req *email.Email, resp *emailApi.Response) error {

	if err := addProfileFromContext(req, &ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not updated")
	}

	emailResponse, err := e.EmailService.Create(ctx, req)

	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not created")
	}


	resp.Email = emailResponse.Email

	return nil
}

func (e *Email) Update(ctx context.Context, req *email.Email, resp *emailApi.Response) error {

	if err := addProfileFromContext(req, &ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not updated")
	}

	campResponse, err := e.EmailService.Update(ctx, req)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not updated")
	}

	resp.Email = campResponse.Email

	return nil
}

func (e *Email) Delete(ctx context.Context, req *emailApi.EmailId, resp *emailApi.Error) error {

	cmp := &email.Email{Id: req.Id}
	if err := addProfileFromContext(cmp, &ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not updated")
	}

	_, err := e.EmailService.Delete(ctx, cmp)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not deleted")
	}

	return nil
}

func (e *Email) Details(ctx context.Context, req *emailApi.EmailId, resp *emailApi.Response) error {

	cmp := &email.Email{Id: req.Id}
	if err := addProfileFromContext(cmp, &ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "email is not updated")
	}

	campResponse, err := e.EmailService.Get(ctx, cmp)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "could not receive email details")
	}

	resp.Email = campResponse.Email

	return nil
}

func (e *Email) CreateBulk(ctx context.Context, stream emailApi.EmailApi_CreateBulkStream) error {

	emailServicePublishClient, err := e.EmailService.Publish(ctx)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.email", "could not create client for publishing emails")
	}

	for {
		req, err := stream.Recv()
		if err != nil {
			fmt.Println(err)
			return merr.BadRequest("go.micro.api.email", "could not receive email in CreateBulk")
		}

		if err = emailServicePublishClient.Send(req); err != nil {
			return err
		}
		err = stream.Send(&emailApi.Response{Email: req})
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
}

func addProfileFromContext(req *email.Email, ctx *context.Context) error {

	prof, ok := (*ctx).Value("Profile").(*profile.Profile)
	if !ok {
		return merr.Unauthorized("go.micro.api.email", "profile not found in Email Handler")
	}
	req.Profile = prof
	return nil
}
