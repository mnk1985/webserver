package main

import (
	"log"

	"bitbucket.org/mnk1985/webserver/api/email/handler"
	"bitbucket.org/mnk1985/webserver/api/wrapper"

	emailApi "bitbucket.org/mnk1985/webserver/api/email/proto"
	email "bitbucket.org/mnk1985/webserver/srv/email/proto"
	"github.com/micro/go-micro"
)

func main() {

	service := micro.NewService(
		micro.Name("go.micro.api.email"),
		micro.WrapHandler(wrapper.AuthWrapper),
	)
	service.Init()
	emailApi.RegisterEmailApiHandler(service.Server(),
		&handler.Email{
			EmailService: email.NewEmailServiceClient("go.micro.srv.email", service.Client()),
		})

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
