package handler

import (
	"context"
	"errors"
	"fmt"

	merr "github.com/micro/go-micro/errors"

	account "bitbucket.org/mnk1985/webserver/api/account/proto"
	auth "bitbucket.org/mnk1985/webserver/srv/auth/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
)

type Account struct {
	ProfileServiceClient profile.ProfileServiceClient
	AuthServiceClient    auth.AuthServiceClient
}

func (acc *Account) Auth(ctx context.Context, cred *profile.ProfileCredentials, token *auth.Token) error {

	profServiceResponse, err := acc.ProfileServiceClient.GetByCreds(ctx, cred)
	if err != nil {
		fmt.Println(err)
		return err
	}

	tokenResp, err := acc.AuthServiceClient.Encode(ctx, profServiceResponse.Profile)

	if err != nil {
		fmt.Println(err)
		return err
	}

	token.Token = tokenResp.Token
	return nil
}

func (acc *Account) Details(ctx context.Context, req *account.EmptyRequest, resp *account.Response) error {
	prof, ok := ctx.Value("Profile").(*profile.Profile)
	if !ok {
		return merr.Unauthorized("go.micro.api.account", "not found profile")
	}

	resp.Profile = prof
	return nil
}

func (acc *Account) Create(ctx context.Context, user *profile.Profile, response *account.Response) error {
	profileServiceResponse, err := acc.ProfileServiceClient.Create(ctx, user)

	if err != nil {
		fmt.Println(err)
		return errors.New(fmt.Sprintf("error creating user: %v", err))
	}

	response.Profile = profileServiceResponse.Profile

	return nil
}
