docker run --rm \
    -v /home/alex/www/go/src/bitbucket.org/mnk1985/webserver:/go/src/bitbucket.org/mnk1985/webserver \
    -w /go/src/bitbucket.org/mnk1985/webserver/api/account \
    iron/go:dev \
    go build -o api-account

docker build -t mnk1985/api-account:latest .

docker run --rm -p 8080:8080 mnk1985/api-account