package main

import (
	"log"

	account "bitbucket.org/mnk1985/webserver/api/account/proto"
	auth "bitbucket.org/mnk1985/webserver/srv/auth/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	"github.com/micro/go-micro"

	"bitbucket.org/mnk1985/webserver/api/account/handler"
	"bitbucket.org/mnk1985/webserver/api/wrapper"
)

func main() {
	service := micro.NewService(
		micro.Name("go.micro.api.account"),
		micro.WrapHandler(wrapper.AuthWrapper),
	)
	service.Init()
	account.RegisterAccountApiHandler(service.Server(),
		&handler.Account{
			ProfileServiceClient: profile.NewProfileServiceClient("go.micro.srv.profile", service.Client()),
			AuthServiceClient:    auth.NewAuthServiceClient("go.micro.srv.auth", service.Client()),
		})

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
