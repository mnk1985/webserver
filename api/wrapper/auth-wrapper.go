package wrapper

import (
	"context"
	auth "bitbucket.org/mnk1985/webserver/srv/auth/proto"

	"errors"

	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/server"
)

var methodsWithoutAuth = map[string]bool{"AccountApi.Auth": true, "AccountApi.Create": true}

func AuthWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, resp interface{}) error {
		method := req.Method()
		if _, ok := methodsWithoutAuth[method]; ok {
			return fn(ctx, req, resp)
		}

		authClient := auth.NewAuthServiceClient("go.micro.srv.auth", client.DefaultClient)

		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return errors.New("no auth meta-data found in request")
		}

		token := meta["Token"]
		newCtx := context.WithValue(ctx, "Method", method)

		_, err := authClient.ValidateToken(newCtx, &auth.Token{Token: token})
		if err != nil {
			return err
		}

		prof, err := authClient.Decode(newCtx, &auth.Token{Token: token})
		if err != nil {
			return err
		}

		newCtxWithProf := context.WithValue(newCtx, "Profile", prof.Profile)

		return fn(newCtxWithProf, req, resp)
	}
}
