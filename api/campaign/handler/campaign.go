package handler

import (
	"context"

	campaignApi "bitbucket.org/mnk1985/webserver/api/campaign/proto"

	campaign "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	profile "bitbucket.org/mnk1985/webserver/srv/profile/proto"
	merr "github.com/micro/go-micro/errors"

	"fmt"
)

type Campaign struct {
	CampaignService campaign.CampaignServiceClient
}

func (camp *Campaign) Create(ctx context.Context, req *campaign.Campaign, resp *campaignApi.Response) error {

	if err := addProfileFromContext(req, ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "profile not added to context")
	}

	campResponse, err := camp.CampaignService.Create(ctx, req)

	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "campaign is not created")
	}

	resp.Campaign = campResponse.Campaign

	return nil
}

func (camp *Campaign) Update(ctx context.Context, req *campaign.Campaign, resp *campaignApi.Response) error {

	if err := addProfileFromContext(req, ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "profile not added to context")
	}

	campResponse, err := camp.CampaignService.Update(ctx, req)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "campaign is not updated")
	}

	resp.Campaign = campResponse.Campaign

	return nil
}

func (camp *Campaign) Delete(ctx context.Context, req *campaignApi.CampaignId, resp *campaignApi.Error) error {

	cmp := &campaign.Campaign{Id: req.Id}
	if err := addProfileFromContext(cmp, ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "profile not added to context")
	}

	_, err := camp.CampaignService.Delete(ctx, cmp)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "campaign is not deleted")
	}

	return nil
}

func (camp *Campaign) Details(ctx context.Context, req *campaignApi.CampaignId, resp *campaignApi.Response) error {

	cmp := &campaign.Campaign{Id: req.Id}

	if err := addProfileFromContext(cmp, ctx); err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "profile not added to context")
	}

	campResponse, err := camp.CampaignService.Get(ctx, cmp)
	if err != nil {
		fmt.Println(err)
		return merr.BadRequest("go.micro.api.campaign", "could not receive campaign details")
	}

	resp.Campaign = campResponse.Campaign

	return nil
}

func addProfileFromContext(req *campaign.Campaign, ctx context.Context) error {
	prof, ok := ctx.Value("Profile").(*profile.Profile)
	if !ok {
		return merr.Unauthorized("go.micro.api.campaign", "profile not found in Campaign Handler")
	}
	req.Profile = prof
	return nil
}
