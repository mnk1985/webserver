package main

import (
	"log"

	"bitbucket.org/mnk1985/webserver/api/campaign/handler"

	campaignApi "bitbucket.org/mnk1985/webserver/api/campaign/proto"
	"bitbucket.org/mnk1985/webserver/api/wrapper"
	campaign "bitbucket.org/mnk1985/webserver/srv/campaign/proto"
	"github.com/micro/go-micro"
)

func main() {

	service := micro.NewService(
		micro.Name("go.micro.api.campaign"),
		micro.WrapHandler(wrapper.AuthWrapper),
	)
	service.Init()
	campaignApi.RegisterCampaignApiHandler(service.Server(),
		&handler.Campaign{
			CampaignService: campaign.NewCampaignServiceClient("go.micro.srv.campaign", service.Client()),
		})

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
